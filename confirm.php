<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./confirm.css">
    <style>
        label::after {
            margin-left: 5px;
            font-size: 20px;
            content: "";
            /* color: red; */
        }
    </style>
</head>


<body id="body">
    <?php

    $folder_img = "folder_img";
    if (!file_exists($folder_img) && !is_dir($folder_img)) {
        mkdir($folder_img);
    }
    $target_dir = "folder_img/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    // if (!file_exists($_FILES['myfile']['tmp_name']) || !is_uploaded_file($_FILES['myfile']['tmp_name'])) {
    //     // echo 'No uploadaaa';
    // } else {
    if (!$_POST["button"]) {
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


        // echo $target_file . "<br/>";

        // echo $_FILES["fileToUpload"]["tmp_name"] . "asdasdasd";
        if (isset($_POST["asdsa"]) && $_FILES["fileToUpload"]["tmp_name"]) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                // echo "File is an image - " . $check["mime"] . ". <br/>";
                $uploadOk = 1;
            } else {
                echo "File is not an image. <br/>";
                $uploadOk = 0;
            }
        }
        // echo file_exists($target_file) . "<br/>";
        // Check if file already exists
        // if ($_FILES["fileToUpload"]["size"] > 500000) {
        //     echo "Sorry, your file is too large.";
        //     $uploadOk = 0;
        // }

        if (file_exists($target_file)) {
            unlink($target_file);
        }

        // echo " : " . file_exists($target_file) . "<br/>";

        // Allow certain file formats
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $_FILES["fileToUpload"]["name"] != ""
        ) {
            echo "<p style = 'margin-left: 2.3cm; color: red; margin-bottom: -12px;'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</p>";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            // echo "Sorry, your file was not uploaded.";
            // exit();
            // if everything is ok, try to upload file
        } else {
            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
            // if () {
            //     // echo "The file " . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " has been uploaded.";
            // } else {
            //     echo "Sorry, there was an error uploading your file.";
            // }
        }
    } else {
        $target_file = $_POST["file_address"];
        echo "<p style = 'text-align: center;'>Thành công</p>";
    }


    // if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //     // collect value of input field
    //     $name = $_POST;
    //     if (empty($name)) {
    //         echo "Name is empty";
    //     } else {
    //         foreach ($name as $key => $value) :
    //             echo "key= " . $key . " ; value = " . $value . "<br/>";
    //         endforeach;
    //     }
    // }

    // }
    ?>


    <div class="container">
        <fieldset class="group_name" style="color : black">
            <label id="name" for="uname" style="color: white;">Họ và tên</label>
            <?php
            echo $_POST["uname"];
            ?>
        </fieldset>

        <fieldset class="group_gender">
            <label id="gender" for="gen">Giới tính</label>
            <?php
            $gender = array(0 => "Nam", 1 => "Nữ");
            $department = array(
                "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu"
            );
            echo $gender[$_POST["gen"]];
            ?>
        </fieldset>

        <fieldset class="group_department">
            <label id="department" for="depart">Phân Khoa</label>
            <?php
            echo $department[$_POST["depart"]];
            ?>
        </fieldset>

        <fieldset class="date_group">
            <label id="date" for="d">Ngày sinh</label>
            <?php
            $date = $_POST["d"];
            echo join("/", array_reverse(explode("-", $date)))
            ?>
            <!-- <input id="date_input" placeholder="Enter Username" data-date="" data-date-format="DD/MM/YYYY" type="date" name="d" max=<?php //echo $day 
                                                                                                                                            ?>> -->
        </fieldset>

        <fieldset class="address_group">
            <label id="add_label" for="add">Địa chỉ</label>
            <div id="addres_info">
                <?php
                echo $_POST["add"];
                ?>
            </div>
            <!-- <input id="add_input" type="text" name="add"> -->

            <!-- <textarea id="add_input" rows="5" cols="42" name="add" wrap="hard"></textarea> -->
        </fieldset>

        <fieldset class="image_group">
            <label id="img_label" for="fileToUpload">Hình ảnh</label>
            <img src=<?php echo $target_file; ?> onerror="this.onerror=null; this.src='default.jpg'" width="200px" height="150px">


            <!-- <input type="file" name="fileToUpload" id="fileToUpload" accept=".jpg, .png, .jpeg"> -->
        </fieldset>

        <form style="border: 0;text-align: center;" method="post" action="list_sv.php">

            <?php
            $name = $_POST;
            if (empty($name)) {
                echo "Name is empty";
            } else {
                foreach ($name as $key => $value) :
                    // echo "key= " . $key . " ; value = " . $value . "<br/>";
                    echo "<input type='hidden' name='{$key}' value='{$value}'>";
                endforeach;
            }
            echo "<input type='hidden' name='file_address' value='{$target_file}'>";

            if (!$_POST["button"]) {
                echo '<button id="my_button" name="button" type="submit" value="confirm">Xác nhận</button>';
            } else {
                // echo "<p style = 'text-align: center;'>Thành công</p>";
                include "database.php";
                $sql = "INSERT INTO SINHVIEN (HoVaTenSV, GioiTinh, MaKH, NgaySinh, DiaChi, HinhAnh)
    VALUES (?, ?, ?, ?, ?, ?);";
                if ($stmt = $conn->prepare($sql)) {
                    $stmt->bind_param(
                        "ssssss",
                        $_POST["uname"],
                        $_POST["gen"],
                        $_POST["depart"],
                        $_POST["d"],
                        $_POST["add"],
                        $_POST["file_address"]
                    );    // "s" string type      
                    $stmt->execute();
                    // $stmt->bind_result($name, $salary, $ssn);
                    // while ($stmt->fetch()) {
                    //     printf("%s %s %s\n", $name, $salary, $ssn);
                    // }
                }
                $conn -> close();
            }
            ?>


        </form>

    </div>

</body>

</html>