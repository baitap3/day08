<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="./list_sv.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>


<body id="body">
    <?php
    require_once "database.php";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ($_POST["button"] == "confirm") {
            $sql = "INSERT INTO SINHVIEN (HoVaTenSV, GioiTinh, MaKH, NgaySinh, DiaChi, HinhAnh)
    VALUES (?, ?, ?, ?, ?, ?);";
            if ($stmt = $conn->prepare($sql)) {
                $stmt->bind_param(
                    "ssssss",
                    $_POST["uname"],
                    $_POST["gen"],
                    $_POST["depart"],
                    $_POST["d"],
                    $_POST["add"],
                    $_POST["file_address"]
                );    // "s" string type      
                $stmt->execute();
    ?>
                <form id="form_temp" action="" method="post">

                </form>
                <script type="text/javascript">
                    document.getElementById('form_temp').submit();
                </script>


    <?php
            }
        }
    }
    ?>

    <div class="container">
        <!-- <p id="test-doc"></p>
        <table id="temp_table"></table> -->
        <?php
        $gender = array(0 => "Nam", 1 => "Nữ");
        $department = array(
            "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu"
        );
        ?>

        <div class="div_serch">
            <fieldset class="group_department">
                <label id="department" for="depart" style="padding-right: 35px;padding-left: 2px;margin-left: -3.25cm;">Khoa</label>
                <select id="select_depart" name="depart">
                    <?php
                    foreach ($department as $key => $value) :
                        if ($key == "nothing") :
                            echo '<option selected="selected" value="' . $key . '">' . $value . '</option>';
                            continue;
                        else :
                            echo $key;
                            echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!
                        endif;
                    endforeach;
                    ?>
                </select>
            </fieldset>

            <fieldset class="key_word">
                <label id="key_word_label" for="key_w" style="margin-left: -3cm;">Từ khóa</label>
                <input id="key_word_input" type="text" name="key_w">
            </fieldset>

            <fieldset class="reset_button" style="text-align: center;">
                <button id="my_reset_button" name="reset_but" type="button" onclick="reset_table()">Reset</button>
            </fieldset>
        </div>

        <fieldset>
            <form action="register.php" method="post">
                <p id="number_student">Số sinh viên tìm thấy: XXX</p>
                <button id="my_insert_button" name="insert_but" type="submit">Thêm</button>
            </form>
        </fieldset>


        <fieldset class="table_display">
            <?php

            $sql = "Select ID, HoVaTenSV, MaKH FROM SINHVIEN";
            $result = $conn->query($sql);
            ?>

            <table id="table_sv">
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th align="left">Khoa</th>
                    <th class="action">Action</th>
                </tr>
                <?php
                if ($result->num_rows > 0) {
                    $index = 0;
                    while ($data = $result->fetch_assoc()) {
                        $index++;
                ?>
                        <tr>

                            <td><?php echo $index; ?> </td>
                            <td><?php echo $data['HoVaTenSV']; ?> </td>
                            <td style="width: 65%; text-align:left;"><?php echo $data['MaKH']; ?> </td>
                            <td class="action" style="width: 15%;">
                                <button class="delete_change" onclick=<?= "document.getElementById('delete_" . $data["ID"] . "').style.display='block'" ?>>Xóa</button>
                                <button class="delete_change" onclick=<?= "document.getElementById('" . $data["ID"] . "').style.display='block'" ?>>Sửa</button>
                            </td>
                        <tr>



                            <!-- The Modal -->
                            <!-- delete -->
                            <div class="modal" id=<?= "delete_" . $data["ID"] ?> style="display: none;">
                                <span onclick=<?= "document.getElementById('delete_" . $data["ID"] . "').style.display='none'" ?> class="close" title="Close Modal">×</span>
                                <div class="modal-content" style="width: 50%;">
                                    <h1>Delete Account</h1>
                                    <p>Bạn có muốn xóa không?</p>
                                    <button type="button" onclick=<?= "document.getElementById('delete_" . $data["ID"] . "').style.display='none'" ?> class="cancelbtn">Cancel</button>
                                    <form style="display: none;" id="form_temp2" action="" method="post">

                                    </form>
                                    <button onclick=<?= "del_row(" . $data["ID"] . ")" ?>>Xác nhận</button>
                                </div>
                            </div>

                            <!-- edit  -->
                            <div class="modal" id=<?= $data["ID"] ?> style="display: none;">
                                <span onclick=<?= "document.getElementById('" . $data["ID"] . "').style.display='none'" ?> class="close" title="Close Modal">×</span>
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Employee</h4>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <form action="update_sv.php">
                                                <input class="form-control" type="hidden" name="id">
                                                <div class="form-group">
                                                    <label for="first_name">First Name</label>
                                                    <input class="form-control" type="text" name="first_name">
                                                </div>

                                                <div class="form-group">
                                                    <label for="address">Address</label>
                                                    <textarea class="form-control" type="text" name="address" rows="3"></textarea>
                                                </div>
                                                <button type="button" onclick=<?= "document.getElementById('" . $data["ID"] . "').style.display='none'" ?> class="cancelbtn">Cancel</button>
                                                <button type="submit">Update</button>

                                            </form>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php

                    }
                } else { ?>
                        <tr>
                            <td>No data found</td>
                        </tr>
                    <?php } ?>
            </table>
        </fieldset>
    </div>



    <script type="text/javascript" src="./list_sv.js"></script>

</body>

</html>