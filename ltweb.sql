-- CREATE DATABASE IF not EXISTS ltweb;

-- USE ltweb;
-- ALTER DATABASE ltweb CHARACTER SET utf8 COLLATE utf8_general_ci;

-- -- DROP TABLE `SINHVIEN`;
-- CREATE Table IF not EXISTS
--     SINHVIEN(
--         ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         HoVaTenSV VARCHAR(30),
--         GioiTinh char(1),
--         MaKH VARCHAR(6),
--         NgaySinh DATE,
--         DiaChi VARCHAR(50),
--         HinhAnh VARCHAR(100)
--     );

-- SELECT * FROM SINHVIEN;

-- DELETE FROM SINHVIEN WHERE ID >=3;

DROP DATABASE IF EXISTS ltweb;

CREATE DATABASE ltweb CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE ltweb;
CREATE Table IF not EXISTS
    SINHVIEN(
        ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        HoVaTenSV VARCHAR(30),
        GioiTinh char(1),
        MaKH VARCHAR(6),
        NgaySinh DATE,
        DiaChi VARCHAR(50),
        HinhAnh VARCHAR(100)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



